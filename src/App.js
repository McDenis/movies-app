import React, { useEffect, useState, Component } from 'react';
import Movie from './components/Movie';

const API_KEY = "18c6457bf177cea4c18ee14ffda6b7ee";
const FEATURED_API = "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=18c6457bf177cea4c18ee14ffda6b7ee&page=1";
const PAGE_API = "https://api.themoviedb.org/3/discover/movie?api_key=18c6457bf177cea4c18ee14ffda6b7ee&page=";
const SEARCH_API = "https://api.themoviedb.org/3/search/movie?&api_key=18c6457bf177cea4c18ee14ffda6b7ee&query=";


function App() {
  const [ movies, setMovies ] = useState([]);
  const [ searchTerm, setSearchTerm ] = useState(''); 
  const [ NumberTerm, setNumberTerm ] = useState(''); 

  useEffect(() => {
    getMovies(FEATURED_API);
  }, [])

  const getMovies = (API) => {
    fetch(API)
      .then(res => res.json())
      .then(data => {
        setMovies(data.results);
      });
  }

  const handleOnSubmit = (e) => {
    e.preventDefault();

    if(searchTerm) {
      getMovies(SEARCH_API+searchTerm);
      setSearchTerm('');
    }
  };

  const handleOnChange = (e) => {
    setSearchTerm(e.target.value);
  };

  /*********************************************/

  const handleOnSubmitPage = (e) => {
    e.preventDefault();

    if(NumberTerm < 1 || NumberTerm > 500) {
      setNumberTerm('');   
    } else {
      getMovies(PAGE_API+NumberTerm);
    }
  };

  const handleOnChangePage = (e) => {
    setNumberTerm(e.target.value);
  };

  /**********************************************/

  return (
    <>
      <header>
        <form onSubmit={handleOnSubmit}>
          <input 
            className="search" 
            type="search" 
            placeholder="Search..." 
            value={searchTerm}
            onChange={handleOnChange}
          />
        </form>
      </header>

      <div className="movie-container">
        {movies.length > 0 && 
          movies.map((movie) => <Movie key={movie.
          id} {...movie} /> )}
      </div>
      
      <footer>
        <form onSubmit={handleOnSubmitPage}>
          <input 
            className="search-page" 
            type="search"
            placeholder="Page number: 5"
            value={NumberTerm}
            onChange={handleOnChangePage}
          />
        </form>
      </footer>
    </>
  );
}

export default App;
